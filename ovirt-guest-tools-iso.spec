# Up to two digits, to fit in the iso label
%global rel 3

%global spice_nsis_version 0.171
%global source_base spice-nsis-%{spice_nsis_version}

Name:		ovirt-guest-tools-iso
Version:	4.3
Release:	%{rel}%{?release_suffix}%{?dist}
Summary:	oVirt Windows Guest Tools
License:	GPLv2 and GPLv2+ and ASL 2.0 and Zlib and MIT and Python and Platform SDK Redistributable EULA and Microsoft DDK Redistributable EULA
Source0:	https://resources.ovirt.org/pub/ovirt-master-snapshot/src/ovirt-wgt/%{source_base}.tar.bz2
URL:		https://www.ovirt.org/develop/release-management/features/integration/windows-guest-tools/
BuildArch:	noarch


BuildRequires:	genisoimage
BuildRequires:	rsync
BuildRequires:	mingw32-nsis >= 3.01

# Drop these when virtio-win includes the drivers not inside an iso.
# See comment in Makefile.
BuildRequires:	p7zip
BuildRequires:	p7zip-plugins
BuildRequires:	hardlink

# From http://www.spice-space.org/download/windows/vdagent/vdagent-win-0.9.0/
BuildRequires:	mingw32-spice-vdagent >= 0.9.0
BuildRequires:	mingw64-spice-vdagent >= 0.9.0

# From http://resources.ovirt.org/pub
BuildRequires:	ovirt-guest-agent-windows
BuildRequires:	vcredist-x86
BuildRequires:	nsis-simple-service-plugin >= 1.30-2

# From https://docs.fedoraproject.org/en-US/quick-docs/creating-windows-virtual-machines-using-virtio-drivers/index.html#installing-virtio-win-repo
BuildRequires:	virtio-win >= 0.1.171

Obsoletes:	ovirt-guest-tools

%description
Windows Guest tools ISO for oVirt Virtualization Manager.

%global make_common_opts \\\
	PREFIX=%{_prefix} \\\
	MODE=OVIRT \\\
	DISPLAYED_VERSION='%{version}-%{release}' \\\
	ISO_LABEL='oVirt-WGT-%{version}-%{rel}'

%prep
%setup -n %{source_base} -q

%build
make %{make_common_opts}

%install
make %{make_common_opts} install DESTDIR="%{buildroot}"

%files
%{_datadir}/%{name}

%changelog
* Tue Mar  3 2020 changjie.fu <changjie.fu@cs2c.com.cn> - 4.3-3
- Package Initialization
